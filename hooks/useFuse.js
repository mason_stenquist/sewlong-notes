import Fuse from 'fuse.js';

const useFuse = (list, search, fuseOptions) => {
  var items;
  if (!search && fuseOptions.returnFullListOnNoSearch) {
    items = list.map((item) => ({ item: item }));
  } else {
    const fuse = new Fuse(list, fuseOptions);
    items = fuse.search(search);
  }
  return items;
};

export default useFuse;
