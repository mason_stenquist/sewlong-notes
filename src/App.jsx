import { createEffect } from 'solid-js';
import { getNotes } from './api/api';
import Buttons from './components/Buttons';
import PopoverButton from './components/PopoverButton';
import LoadingIcon from './assets/LoadingIcon';
import Search from './components/Search';
import Table from './components/Table';
import {
  clientNameSelect,
  completedBySelect,
  dateFilter,
  isFetching,
  isLoading,
  projectNumber,
  recordNumber,
  setSkip,
  skip,
  statusSelect,
  tagSelect,
  typeSelect
} from './store';
import { TagModal } from './components/TagModal';

const { showQuickFilters } = FM_CONFIG;

function App() {
  createEffect(() => {
    //DEPS
    typeSelect();
    statusSelect();
    completedBySelect();
    clientNameSelect();
    recordNumber();
    projectNumber();
    dateFilter();
    tagSelect();
    getNotes(true);
  });

  const refresh = () => {
    setSkip(0);
    getNotes(false);
  };

  //Setup Infinite Load
  let loadMore;
  createEffect(() => {
    if (!isLoading()) {
      const observer = new IntersectionObserver(() => setSkip(skip() + 1), {
        rootMargin: '100px',
        threshold: 1.0
      });
      if (loadMore) {
        observer.observe(loadMore);
      }
    }
  });

  const reloadClasses = () => ({
    'border rounded-full w-10 h-10  border-blue-700 hover:bg-blue-100 text-blue-700 text-xl pb-1': true,
    'animate-spin': isFetching()
  });

  return (
    <div class="">
      {isLoading() && (
        <span className="bg-white bg-opacity-20 pulse fixed top-0 left-0 h-screen w-full z-30 flex items-center justify-center">
          <LoadingIcon />
        </span>
      )}
      <div class="flex justify-between border-b fixed w-full bg-white p-4 items-center z-20">
        <div class="flex space-x-2">
          {showQuickFilters && <PopoverButton title="Quick Filters" />}
          <Buttons />
        </div>

        <div className="flex space-x-2 items-center">
          <Search />
          <button classList={reloadClasses()} onClick={refresh}>
            ⟳
          </button>
        </div>
      </div>
      <Table />
      <TagModal />
      <div ref={loadMore}></div>
    </div>
  );
}

export default App;
