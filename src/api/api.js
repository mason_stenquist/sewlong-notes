import { fmScript } from 'fmcperformscript';
import { ALL } from '../components/TableHeader';
import {
  clientNameOptions,
  clientNameSelect,
  completedByOptions,
  completedBySelect,
  dateFilter,
  notes,
  projectNumber,
  recordNumber,
  setClientNameOptions,
  setCompletedByOptions,
  setIsFetching,
  setIsLoading,
  setNotes,
  setStatusOptions,
  setTagOptions,
  setTypeOptions,
  skip,
  sortDirection,
  sortField,
  statusOptions,
  statusSelect,
  tagOptions,
  tagSelect,
  typeOptions,
  typeSelect,
  hasInitData,
  setHasInitData,
  bustCache,
  setBustCache,
  quickFilter
} from '../store';
import { fmFind } from './fmFind';

const LIMIT = 50;
const { query } = FM_CONFIG;

export const getNotes = async (useCache = true) => {
  if (quickFilter()) {
    return;
  }

  setIsFetching(true);
  const fullQuery = { ...query };
  if (typeSelect()) fullQuery.type = typeSelect();
  if (statusSelect()) fullQuery.status = statusSelect();
  if (completedBySelect()) fullQuery.Completed_By = completedBySelect();
  if (tagSelect()) fullQuery.tags = tagSelect();
  if (clientNameSelect())
    fullQuery['note_CLIENT_customer::Name__Client'] = clientNameSelect();
  if (recordNumber()) fullQuery['note_RECORD::z___Number'] = recordNumber();
  if (projectNumber()) fullQuery['note_PROJECT::z___Number'] = projectNumber();
  if (dateFilter()) fullQuery.zLog_Create_TS = dateFilter();

  //Clean Query (Remove *ALL* values)
  const queryArray = Object.entries(fullQuery);
  const updatedQuery = queryArray
    .filter(([_key, value]) => value !== ALL)
    .reduce((acc, [key, value]) => ({ ...acc, [key]: value }), {});

  const params = {
    limit: LIMIT,
    offset: skip() * LIMIT + 1,
    query: [updatedQuery],
    sort: [{ fieldName: sortField(), sortOrder: sortDirection() }]
  };

  let results;
  try {
    results = await fmFind(
      'Note-JSON',
      params,
      !bustCache() && useCache,
      1000 * 60 * 5
    );
    setBustCache(false);
  } catch (e) {
    console.log(e);
    results = { response: { data: [] } };
  }
  const records = (results?.response?.data ?? []).map((record) =>
    JSON.parse(record.fieldData.zctNoteJSON)
  );

  //GET INFO FOR DROPDOWNS
  setTypeOptions(getUniqueByKey(records, 'type', typeOptions()));
  setStatusOptions(getUniqueByKey(records, 'status', statusOptions()));
  setCompletedByOptions(
    getUniqueByKey(records, 'completedBy', completedByOptions())
  );
  setClientNameOptions(
    getUniqueByKey(records, 'clientName', clientNameOptions())
  );

  setTagOptions(getUniqueTags(records, tagOptions()));

  const finalRecords =
    skip() === 0 || hasInitData() ? records : [...notes(), ...records];
  setNotes(finalRecords);
  setIsLoading(false);
  setIsFetching(false);
  setHasInitData(false);
};

export const goToRecord = async (layoutID, field, value) => {
  try {
    await fmScript('fm-bridge-goto', { layoutID, field, value });
  } catch (e) {
    alert(e);
  }
};

const getUniqueByKey = (records, key, existingValues = []) => {
  const keyArray = records.map((record) => record[key]).filter(Boolean);
  const allValues = [...existingValues, ...keyArray];
  return [...new Set(allValues)];
};

const getUniqueTags = (records, existingValues = []) => {
  let allValues = [...existingValues];
  records.forEach((record) => {
    if (record.tags) {
      const tagsArray = record.tags.split('\r');
      allValues = [...allValues, ...tagsArray];
    }
  });
  return [...new Set(allValues)];
};

window.reload = () => getNotes(false);
