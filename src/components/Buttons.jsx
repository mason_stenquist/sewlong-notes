import { fmScript } from 'fmcperformscript';
import { For } from 'solid-js';
import { getNotes } from '../api/api';

const Buttons = () => {
  const buttons = FM_CONFIG.buttons;
  const runScript = async (scriptID, params) => {
    try {
      await fmScript(scriptID, params);
      getNotes(false);
    } catch (e) {
      alert(e);
    }
  };
  return (
    <For each={buttons}>
      {({ label, scriptID, params }) => (
        <button
          class="rounded border px-3 py-2 hover:bg-gray-50 transition-all duration-200"
          onClick={() => runScript(scriptID, params)}
        >
          {label}
        </button>
      )}
    </For>
  );
};

export default Buttons;
