import { createMemo } from 'solid-js';

const regTagName = '?<!</?[^>]*|&[^;]*';

const Highlighted = ({ text = '', highlight = '' }) => {
  if (!highlight.trim()) {
    return <span innerHTML={text}></span>;
  }
  try {
    const regex = new RegExp(`(${regTagName})(${highlight})`, 'gi');
    const parts = text.split(regex);
    const marked = createMemo(() =>
      parts
        .filter((part) => part)
        .map((part, i) =>
          regex.test(part) ? `<mark>${part}</mark>` : `<span>${part}</span>`
        )
    );
    const html = () => marked().join('');
    return <span innerHTML={html()}></span>;
  } catch (e) {
    console.log(e);
    return <span innerHTML={text}></span>;
  }
};

export default Highlighted;
