import { evaluate } from 'fmcperformscript';
import { createEffect, createSignal, onCleanup, Show } from 'solid-js';
import {
  changeQuickFilter,
  quickFilter,
  setCompletedBySelect,
  setQuickFilter
} from '../store';

const PopoverButton = (props) => {
  const [open, setOpen] = createSignal(false);

  const close = () => {
    setOpen(false);
  };

  createEffect(() => {
    if (open()) {
      document.body.addEventListener('click', close);
    }
    onCleanup(() => document.body.removeEventListener('click', close));
  });

  return (
    <div class="relative flex items-center space-x-4">
      <button
        onClick={() => setOpen((old) => !old)}
        class="relative border rounded p-2 divide-x flex items-center"
      >
        <span class="pr-3">{props.title}</span>
        <span class="pl-2 text-[10px] text-gray-400">&#9660;</span>
      </button>
      <Show when={quickFilter()}>
        <button
          onClick={() => setQuickFilter('')}
          class="bg-gray-50 hover:bg-blue-50 px-1 hover:border-blue-600 border p-1 rounded-full flex items-center"
        >
          {quickFilter()} &nbsp;
          <div class="p-0.5 px-2">&times;</div>
        </button>
      </Show>
      <Show when={open()}>
        <div class="absolute left-0 top-[50px] w-52">
          <div class="relative">
            <div class="bg-gray-50 border-l border-t rotate-45 w-4 h-4 absolute top-[-8px] translate-x-[20px]"></div>
            <div class="bg-gray-50 shadow rounded border p-2.5">
              <div class="flex flex-col divide-y justify-start">
                <button
                  onClick={() => changeQuickFilter('(My Recent)')}
                  class="text-left hover:text-blue-700 p-2"
                >
                  My Recent
                </button>
                <button
                  onClick={() => changeQuickFilter('(My To Do)')}
                  class="text-left hover:text-blue-700 p-2"
                >
                  My To Do
                </button>
              </div>
            </div>
          </div>
        </div>
      </Show>
    </div>
  );
};
export default PopoverButton;
