import { search, setExpandNotes, setSearch } from '../store';

const Search = () => {
  const updateSearch = (e) => {
    if (e.target.value.trim() !== '') {
      setExpandNotes(true);
    }
    setSearch(e.target.value);
  };

  return (
    <input
      class="p-2 border rounded-full shadow-inner focus:outline-none focus:ring"
      type="search"
      placeholder="Search..."
      value={search()}
      onInput={(e) => updateSearch(e)}
    />
  );
};

export default Search;
