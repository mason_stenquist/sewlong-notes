import { batch, Show } from 'solid-js';
import { SORT_DIRECTION } from '../constants';
import { search, setSkip, setSort, sortDirection, sortField } from '../store';

const SortButton = (props) => (
  <button
    onClick={() => {
      batch(() => {
        setSkip(0);
        setSort(props.sortKey);
      });
    }}
    class="flex items-center space-x-1 py-2 hover:text-blue-600"
  >
    <span class="font-bold">{props.children}</span>
    <Show when={props.sortKey === sortField() && !search()}>
      <Show when={sortDirection() === SORT_DIRECTION.ASC}>
        <span className="text-xs">&#9660;</span>
      </Show>
      <Show when={sortDirection() === SORT_DIRECTION.DESC}>
        <span className="text-xs">&#9650;</span>
      </Show>
    </Show>
  </button>
);

export default SortButton;
