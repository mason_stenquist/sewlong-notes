import { For, Show } from 'solid-js';
import { isFetching, isLoading, notes, search } from '../store';
import { createMemo } from 'solid-js';
import useFuse from '../../hooks/useFuse';
import { FUSE_CONFIG } from '../constants';

import TableHeaders from './TableHeaders';
import TableRow from './TableRow';

const Table = () => {
  const filteredNotes = createMemo(() => {
    const items = useFuse(notes(), search(), FUSE_CONFIG);
    return items;
  });

  return (
    <div class="pt-[70px]">
      <table class="w-full">
        <thead class="sticky top-[70px] z-10 bg-gray-100 border-b-2">
          <tr class="border-b align-top text-left text-sm pl-4">
            <TableHeaders />
          </tr>
        </thead>
        <tbody>
          <For each={filteredNotes()}>
            {({ item }) => <TableRow item={item} />}
          </For>
        </tbody>
      </table>

      <Show when={isLoading()}>
        <div class="flex items-center justify-center h-screen mt-[-130px]">
          Loading...
        </div>
      </Show>

      <Show when={filteredNotes().length === 0 && !isFetching()}>
        <div class="flex items-center justify-center h-screen mt-[-130px]">
          Nothing to see here
        </div>
      </Show>
    </div>
  );
};

export default Table;
