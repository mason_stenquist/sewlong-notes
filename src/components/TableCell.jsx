const TableCell = (props) => {
  const { hide } = FM_CONFIG;
  if ((hide || []).includes(props.key)) return null;
  return <td>{props.children}</td>;
};

export default TableCell;
