import { batch, For } from 'solid-js';
import { setQuickFilter, setSkip } from '../store';

import SortButton from './SortButton';

export const ALL = '*ALL*';

const Select = (props) => {
  const options = () => FM_CONFIG?.options?.[props.key] ?? props.options;

  return (
    <select
      onChange={(e) => props.onChange(e.target.value)}
      class={`h-6 border rounded mb-1 ${props.width}`}
    >
      <option value={ALL}>All</option>
      <For each={options()}>
        {(option) => <option value={option}>{option}</option>}
      </For>
    </select>
  );
};

const Input = (props) => {
  return (
    <input
      type="search"
      onChange={(e) => props.onChange(e.target.value)}
      class={`pl-1 h-6 border rounded mb-1 ${props.width}`}
    />
  );
};

const TableHeader = (props) => {
  const { hide } = FM_CONFIG;
  const { colspan = 1 } = props;
  const handleChange = (value) => {
    batch(() => {
      setQuickFilter('');
      props.onChange(value);
      setSkip(0);
    });
  };
  if ((hide || []).includes(props.key)) return null;
  return (
    <th colspan={colspan} class={`${props.width}`}>
      <SortButton sortKey={props.sortKey}>{props.label}</SortButton>
      {props.options ? (
        <Select {...props} onChange={handleChange} />
      ) : (
        <Input {...props} onChange={handleChange} />
      )}
    </th>
  );
};

export default TableHeader;
