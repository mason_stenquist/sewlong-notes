import {
  typeOptions,
  setTypeSelect,
  setStatusSelect,
  statusOptions,
  setCompletedBySelect,
  completedByOptions,
  setClientNameSelect,
  clientNameOptions,
  setRecordNumber,
  setProjectNumber,
  setDateFilter,
  setTagSelect,
  tagOptions,
  setExpandNotes,
  expandNotes,
  setExpandedIds
} from '../store';

import TableHeader from './TableHeader';

const TableHeaders = () => {
  const unExpandAll = () => {
    setExpandNotes(false);
    setExpandedIds([]);
  };

  return (
    <>
      <TableHeader
        width="w-20"
        wrapperClasses="pl-4"
        key="date"
        sortKey="zLog_Create_TS"
        label="Date"
        onChange={setDateFilter}
      />
      <TableHeader
        width="w-24"
        wrapperClasses="pl-4"
        key="type"
        sortKey="Type"
        label="Type"
        onChange={setTypeSelect}
        options={typeOptions()}
      />

      <TableHeader
        width="w-20"
        key="status"
        sortKey="Status"
        label="Status"
        onChange={setStatusSelect}
        options={statusOptions()}
      />

      <TableHeader
        width="w-32"
        key="completedBy"
        sortKey="Completed_By"
        label="Completed By"
        onChange={setCompletedBySelect}
        options={completedByOptions()}
      />

      <TableHeader
        width="w-36"
        key="clientName"
        sortKey="note_CLIENT_customer::Name__Client"
        label="Client Name"
        onChange={setClientNameSelect}
        options={clientNameOptions()}
      />

      <TableHeader
        width="w-20"
        key="recordNumber"
        sortKey="note_RECORD::z___Number"
        label="Record #"
        onChange={setRecordNumber}
      />

      <TableHeader
        width="w-20"
        key="projectNumber"
        sortKey="note_PROJECT::z___Number"
        label="Project #"
        onChange={setProjectNumber}
      />

      <th class="pt-2.5">
        <span className="block pb-2">Note</span>
        {!expandNotes() && (
          <button
            onclick={() => setExpandNotes(true)}
            title="expand rows"
            class="border text-blue-600 px-1 border-blue-600 h-5.5 flex items-center justify-center hover:bg-blue-100"
          >
            + Expand
          </button>
        )}
        {expandNotes() && (
          <button
            onclick={unExpandAll}
            title="collapse rows"
            class="border text-blue-600 px-1 border-blue-600 h-5.5 flex items-center  justify-center hover:bg-blue-100"
          >
            - Collapse
          </button>
        )}
      </th>

      <TableHeader
        width="w-32"
        key="tags"
        sortKey="tags"
        label="Tags"
        colspan="2"
        onChange={setTagSelect}
        options={tagOptions()}
      />
    </>
  );
};

export default TableHeaders;
