import { Show, splitProps } from 'solid-js';
import {
  expandNotes,
  isInExpandedList,
  search,
  toggleExpandedId
} from '../store';

import { getNotes, goToRecord } from '../api/api';
import {
  CLIENT_LAYOUT,
  OPEN_NOTE_MODAL_SCRIPT,
  PROJECT_LAYOUT,
  RECORD_LAYOUT
} from '../constants';
import Highlighted from './Highlighted';

import Tags from './Tags';
import { fmScript } from 'fmcperformscript';
import TableCell from './TableCell';

const TableRow = (props) => {
  const [{ item }] = splitProps(props, ['item']);
  const edit = async (id) => {
    await fmScript(OPEN_NOTE_MODAL_SCRIPT, { noteID: id });
    getNotes(false);
  };

  const isExpanded = () => {
    return expandNotes() || isInExpandedList(item.id);
  };

  const notesClass = () => ({
    ' transition-all duration-300': true,
    'overflow-hidden max-h-6': !isExpanded(),
    'min-h-[125px] max-h-[1000px]': isExpanded()
  });

  const buttonClass = () => ({
    'text-left': true,
    'hover:underline': !isExpanded()
  });

  return (
    <tr class="border-b py-2 align-top text-sm even:bg-gray-50">
      <TableCell key="date">
        <div class="relative">
          {item.date}

          <Show when={isExpanded()}>
            <div class="absolute left-0 w-[400px] top-[35px] text-xs">
              <div class="space-y-2 bg-white rounded shadow-inner p-2 border max-h-24 overflow-y-auto">
                <Show when={item.emailSubject}>
                  <p>
                    <strong>Subject:</strong> {item.emailSubject}
                  </p>
                </Show>
                <Show when={item.emailTo}>
                  <p>
                    <strong>To:</strong> {item.emailTo}
                  </p>
                </Show>
                <Show when={item.emailCC}>
                  <p>
                    <strong>CC:</strong> {item.emailCC}
                  </p>
                </Show>
                <Show when={item.emailBCC}>
                  <p>
                    <strong>BCC:</strong> {item.emailBCC}
                  </p>
                </Show>
                <Show when={item.textTo}>
                  <p>
                    <strong>SMS:</strong> {item.emailTextTo}
                  </p>
                </Show>
              </div>
            </div>
          </Show>
        </div>
      </TableCell>
      <TableCell key="type">{item.type}</TableCell>
      <TableCell key="status">{item.status}</TableCell>
      <TableCell key="completedBy">{item.completedBy}</TableCell>
      <TableCell key="clientName">
        <button
          title="Go To Client"
          class="btn-link hover:text-[#81C234]"
          onClick={() =>
            goToRecord(CLIENT_LAYOUT, 'CLIENT::z___UUID', item.clientUUID)
          }
        >
          {item.clientName}
        </button>
      </TableCell>
      <TableCell key="recordNumber">
        <button
          title="Go To Record"
          class="btn-link hover:text-[#2F85FA]"
          onClick={() =>
            goToRecord(RECORD_LAYOUT, 'RECORD::z___UUID', item.recordUUID)
          }
        >
          {item.recordNumber}
        </button>
      </TableCell>
      <TableCell key="projectNumber">
        <button
          title="Go To Project"
          class="btn-link hover:text-[#FF00FF]"
          onClick={() =>
            goToRecord(PROJECT_LAYOUT, 'PROJECT::z___UUID', item.projectUUID)
          }
        >
          {item.projectNumber}
        </button>
      </TableCell>
      <td>
        <Show when={!isExpanded()}>
          <button
            onClick={() => toggleExpandedId(item.id)}
            classList={buttonClass()}
            title={isExpanded() ? null : item.noteText}
          >
            {item?.noteText?.substring(0, 150)}&hellip;
            <span className="text-blue-600 underline">See more</span>
          </button>
        </Show>
        <div
          classList={notesClass()}
          title={isExpanded() ? null : item.noteText}
        >
          <Show when={isExpanded()}>
            <button
              className="text-blue-600 underline"
              onClick={() => toggleExpandedId(item.id)}
            >
              See less
            </button>
            <div class="relative">
              <Highlighted text={item.note} highlight={search()} />
            </div>
          </Show>
        </div>
      </td>
      <td>
        <Tags tags={item.tags} id={item.id} />
      </td>
      <td class="text-right pr-4">
        <button
          onClick={() => edit(item.id)}
          class="underline hover:text-blue-600  text-right pt-2 pl-1 text-xs"
        >
          Edit
        </button>
      </td>
    </tr>
  );
};

export default TableRow;
