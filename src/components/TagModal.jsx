import { fmScript } from 'fmcperformscript';
import { createEffect, Show } from 'solid-js';
import { TOGGLE_TAG_SCRIPT } from '../constants';
import { tagInfo, setTagInfo, tagOptions, addNoteTag } from '../store';

export const TagModal = () => {
  let inputRef;

  const filteredTags = () =>
    tagOptions().filter((tag) => !tagInfo().existingTags.includes(tag));

  createEffect(() => {
    if (tagInfo()) {
      inputRef.focus();
    }
  });

  const handleSelect = (e) => {
    if (e.key === 'Enter' || e.key === 'Tab' || !e.key) {
      setTimeout(() => {
        addNoteTag(tagInfo().id, inputRef.value);
        fmScript(TOGGLE_TAG_SCRIPT, {
          tag: inputRef.value,
          id: tagInfo().id
        });
        inputRef.value = '';
        setTagInfo(null);
      }, 0);
    }
  };

  return (
    <Show when={tagInfo()}>
      <div
        class="w-[330px] absolute z-20 bg-white shadow-lg border p-1 rounded"
        style={{
          top: `${tagInfo().y}px`,
          left: `${tagInfo().x - 280}px`
        }}
      >
        <input
          ref={inputRef}
          class="border w-full rounded p-3"
          placeholder="Add Tag"
          type="text"
          list="options"
          onKeyDown={handleSelect}
          name="tagselect"
          onBlur={() => setTagInfo(null)}
        />

        <datalist id="options">
          <For each={filteredTags()}>
            {(option) => <option>{option}</option>}
          </For>
        </datalist>
      </div>
    </Show>
  );
};
