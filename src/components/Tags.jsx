import { fmScript } from 'fmcperformscript';
import { For } from 'solid-js';
import { TOGGLE_TAG_SCRIPT } from '../constants';
import { removeNoteTag, setTagInfo } from '../store';

const Tags = (props) => {
  const tags = () => props?.tags?.split('\r')?.filter(Boolean);
  let buttonRef;

  const toggleModal = (e) => {
    const { x, y, height } = buttonRef.getBoundingClientRect();
    setTagInfo({ x, y: y + height, existingTags: tags(), id: props.id });
  };

  const handleClick = async (tag) => {
    try {
      fmScript(TOGGLE_TAG_SCRIPT, {
        tag,
        id: props.id
      });
      removeNoteTag(props.id, tag);
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <div className="flex space-x-1 py-2 text-xs items-start">
      <button
        ref={buttonRef}
        onClick={toggleModal}
        title="Add Tag"
        className=" border-blue-600 border text-blue-600 px-1.5 py-0.5 rounded"
      >
        +{tags().length === 0 && <span>&nbsp;Add Tag</span>}
      </button>
      <For each={tags()}>
        {(tag) => (
          <button
            onClick={() => handleClick(tag)}
            className="bg-blue-600 text-white px-1.5 py-0.5 rounded"
          >
            {tag}&nbsp;<span>&times;</span>
          </button>
        )}
      </For>
    </div>
  );
};

export default Tags;
