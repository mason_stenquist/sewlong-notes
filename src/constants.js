export const PROJECT_LAYOUT = 74;
export const RECORD_LAYOUT = 68;
export const CLIENT_LAYOUT = 48;

export const OPEN_NOTE_MODAL_SCRIPT = 862;

export const TOGGLE_TAG_SCRIPT = 949;

export const QUICK_FILTER_SCRIPT = 953;

export const SORT_DIRECTION = {
  DESC: 'descend',
  ASC: 'ascend'
};

export const FUSE_CONFIG = {
  returnFullListOnNoSearch: true,
  includeMatches: true,
  threshold: 0.1,
  findAllMatches: false,
  ignoreLocation: true,
  minMatchCharLength: 3,
  keys: ['note', 'completedBy', 'clientName', 'recordNumber', 'projectNumber']
};
