import { fmScript } from 'fmcperformscript';
import { createSignal } from 'solid-js';
import { QUICK_FILTER_SCRIPT, SORT_DIRECTION } from './constants';

const ALL = '*ALL*';
const { initData, initDropdowns, initExpanded } = FM_CONFIG;

export const [hasInitData, setHasInitData] = createSignal(
  Array.isArray(initData) && initData.length > 0 ? true : false
);

export const [isLoading, setIsLoading] = createSignal(
  !!initData ? false : true
);

export const [isFetching, setIsFetching] = createSignal(
  !!initData ? false : true
);

export const [bustCache, setBustCache] = createSignal(false);

export const [skip, setSkip] = createSignal(0);

// NOTES
export const [notes, setNotes] = createSignal(initData || []);

export const addNoteTag = (id, tag) => {
  const noteIndex = notes().findIndex((note) => note.id === id);
  const existingTags = notes()[noteIndex].tags.split('\r').filter(Boolean);
  const uniqueTags = Array.from(new Set([...existingTags, tag]));
  const tagString = uniqueTags.join('\r');
  let updatedNotes = structuredClone(notes());
  updatedNotes[noteIndex].tags = tagString;
  setNotes(updatedNotes);
  setBustCache(true);
};

export const removeNoteTag = (id, tag) => {
  const noteIndex = notes().findIndex((note) => note.id === id);
  const tags = notes()
    [noteIndex].tags.split('\r')
    .filter(Boolean)
    .filter((t) => t !== tag);
  const tagString = tags.join('\r');
  let updatedNotes = structuredClone(notes());
  updatedNotes[noteIndex].tags = tagString;
  setNotes(updatedNotes);
  setBustCache(true);
};

export const [search, setSearch] = createSignal('');
export const [expandNotes, setExpandNotes] = createSignal(
  initExpanded || false
);
export const [sortField, setSortField] = createSignal('zLog_Create_TS');
export const [sortDirection, setSortDirection] = createSignal(
  SORT_DIRECTION.DESC
);

const toggleSort = () => {
  if (sortDirection() === SORT_DIRECTION.ASC) {
    setSortDirection(SORT_DIRECTION.DESC);
  } else {
    setSortDirection(SORT_DIRECTION.ASC);
  }
};

export const setSort = (sortKey) => {
  if (sortKey === sortField()) {
    toggleSort();
  } else {
    setSortField(sortKey);
    setSortDirection(SORT_DIRECTION.ASC);
  }
};

export const [typeOptions, setTypeOptions] = createSignal(
  initDropdowns?.typeOptions ?? []
);
export const [typeSelect, setTypeSelect] = createSignal(ALL);

export const [statusOptions, setStatusOptions] = createSignal(
  initDropdowns?.statusOptions ?? []
);
export const [statusSelect, setStatusSelect] = createSignal(ALL);

export const [completedByOptions, setCompletedByOptions] = createSignal(
  initDropdowns?.completedByOptions ?? []
);
export const [completedBySelect, setCompletedBySelect] = createSignal(ALL);

export const [clientNameOptions, setClientNameOptions] = createSignal(
  initDropdowns?.clientNameOptions ?? []
);
export const [clientNameSelect, setClientNameSelect] = createSignal(ALL);

export const [tagOptions, setTagOptions] = createSignal(
  initDropdowns?.tagOptions ?? []
);
export const [tagSelect, setTagSelect] = createSignal(ALL);

export const [recordNumber, setRecordNumber] = createSignal('');
export const [projectNumber, setProjectNumber] = createSignal('');
export const [dateFilter, setDateFilter] = createSignal('');

export const [quickFilter, setQuickFilter] = createSignal('');

export const [expandedIds, setExpandedIds] = createSignal([]);
export const isInExpandedList = (id) => {
  return expandedIds().includes(id);
};
export const toggleExpandedId = (id) => {
  if (isInExpandedList(id)) {
    setExpandedIds((prev) => prev.filter((noteId) => noteId !== id));
  } else {
    setExpandedIds((prev) => [...prev, id]);
  }
};

export const [tagInfo, setTagInfo] = createSignal(null);

export const clearAllFilters = () => {
  setTypeSelect(ALL);
  setStatusSelect(ALL);
  setCompletedBySelect(ALL);
  setTagSelect(ALL);
  setClientNameSelect(ALL);
  setRecordNumber('');
  setProjectNumber('');
  setDateFilter('');
};

export const changeQuickFilter = async (query) => {
  clearAllFilters();
  const notesJSON = await fmScript(QUICK_FILTER_SCRIPT, { query });
  setQuickFilter(query);
  setNotes(notesJSON);
  console.log(notesJSON);
};
