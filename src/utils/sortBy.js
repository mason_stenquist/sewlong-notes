import { SORT_DIRECTION } from '../constants';

export const sortBy = (list, sortField, sortDirection) => {
  if (!sortField) {
    return list;
  }
  const sortChange = sortDirection === SORT_DIRECTION.ASC ? 1 : -1;
  const hasValue = [];
  const noValue = [];
  list.forEach((note) => {
    if (note[sortField] === '') {
      noValue.push(note);
    } else {
      hasValue.push(note);
    }
  });
  const sorted = hasValue.sort((a, b) => {
    if (a[sortField].toUpperCase() < b[sortField].toUpperCase()) {
      return -1 * sortChange;
    }
    if (a[sortField].toUpperCase() > b[sortField].toUpperCase()) {
      return 1 * sortChange;
    }
    return 0;
  });
  return [...sorted, ...noValue];
};
